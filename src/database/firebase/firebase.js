// import * as firebase from 'firebase';
import firebase from 'firebase/app'
import "firebase/database";

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

export const firebaseConfig = {
  apiKey: "AIzaSyBOd1q5foeAGm_yfJiWgH-l1tvd5rw_wFI",
  authDomain: "kingdomexpenses.firebaseapp.com",
  projectId: "kingdomexpenses",
  storageBucket: "kingdomexpenses.appspot.com",
  messagingSenderId: "276343241834",
  appId: "1:276343241834:web:266abdc42349758157de13",
  measurementId: "G-8XYWLDKX6Z",
  databaseURL: "https://kingdomexpenses-default-rtdb.asia-southeast1.firebasedatabase.app",
};

// export const firebaseConfig = {
//   apiKey: "AIzaSyCHGOg6zhT96i4YiFMqk_kRacXr_VryqqM",
//   authDomain: "homodemo-a72ec.firebaseapp.com",
//   databaseURL: "https://homodemo-a72ec.firebaseio.com",
//   projectId: "homodemo-a72ec",
//   storageBucket: "homodemo-a72ec.appspot.com",
//   messagingSenderId: "156508141120",
//   appId: "1:156508141120:web:772171bdb7b56feb8e51a5"
// }

//FirebaseApp.initializeApp();
if (!firebase.apps.length) { 
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

export default firebase;

export const rootRef = firebase.database().ref();
export const walletRef = rootRef.child('Wallet');
export const categoryRef = rootRef.child('Category');
export const userRef = rootRef.child('users');
